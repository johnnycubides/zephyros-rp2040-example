PATH_TOOLS?=
include ../tools/Makefile
BOARD=rpi_pico
DTS_OVERLAY?=./boards/rp2040.dts
SERIAL?=/dev/ttyACM0

environment:
	/bin/bash $(PATH_TOOLS)/zephyr-env.sh default

flash:
	cp build/zephyr/zephyr.uf2 /media/`whoami`/RPI-RP2/
